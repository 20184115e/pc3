package pe.uni.jesusramirezm.pc3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;

public class GameActivity extends AppCompatActivity {

    Button button_erase;
    // Por alguna razón se crean clases en vez de variables y no pude continuar...
    public Button getButton_erase() {
        return button_erase;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
    }
}