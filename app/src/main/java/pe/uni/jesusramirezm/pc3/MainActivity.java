package pe.uni.jesusramirezm.pc3;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.RadioButton;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {

    RadioButton radioButtonTwo, radioButtonThree, radioButtonFour;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        radioButtonTwo = findViewById(R.id.radio_button_two);
        radioButtonThree = findViewById(R.id.radio_button_three);
        radioButtonFour = findViewById(R.id.radio_button_four);
        button = findViewById(R.id.button_start);

        button.setOnClickListener(v -> {

            // Si no ha escogido ninguna opción
            if (!radioButtonTwo.isChecked() && !radioButtonThree.isChecked() && !radioButtonFour.isChecked()) {
                Snackbar.make(v, R.string.snack_bar_msg, Snackbar.LENGTH_LONG).show();
                return ;
            }

            // Si escoge una calculadora no implementada
            if (radioButtonThree.isChecked() || radioButtonFour.isChecked()) {

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("");
                builder.setCancelable(false);
                // Mensaje que mostraremos
                builder.setMessage(R.string.builder_msg_);

                // Respuesta positiva
                builder.setPositiveButton("Sí", (dialog, which) -> {

                // Continuar en la actividad Main (Layout 2)
                    dialog.cancel();
                });
                // Respuesta negativa
                builder.setNegativeButton("No", (dialog, which) -> {

                    // Proceso para cerrar la aplicación
                    moveTaskToBack(true);
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(1);
                }).show();

                builder.create();

            }

            else {
                // Para ir a siguiente actividad
                Intent intent = new Intent(MainActivity.this, GameActivity.class);

                // Si escoge la calculadora básica
                if (radioButtonTwo.isChecked()) {
                    intent.putExtra("TWO", true);
                }

                startActivity(intent);
                // Ya que podremos volver a esta actividad, no la terminamos
            }
        });
    }
}