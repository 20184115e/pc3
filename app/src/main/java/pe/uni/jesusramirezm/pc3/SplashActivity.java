package pe.uni.jesusramirezm.pc3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;


public class SplashActivity extends AppCompatActivity {

    ImageView imageViewSplash;


    Animation animationImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // Enlazando animación
        imageViewSplash = findViewById(R.id.image_view_splash);
        animationImage = AnimationUtils.loadAnimation(this, R.anim.image_animation);
        imageViewSplash.setAnimation(animationImage);

        // Timer 5 segundos
        new CountDownTimer(5000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {

                // Intent para pasar a otra actividad. En este caso, Main.
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }

        }.start();

    }
}